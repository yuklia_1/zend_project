<?php

namespace User;

use User\Model\PasswordRecovery;
use User\Model\User;
use User\Model\UserTable;
use User\Model\PasswordRecoveryTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;
use  User\Model\AuthStorage as AuthStorage;
use Zend\Debug\Debug;
use  Zend\I18n\Translator\Translator as Translator;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions as SmtpOptions;
use Zend\Mail\Message as Message;
use Zend\Mvc\MvcEvent;
use Zend\Session\Container;
use Zend\Crypt\Password\Bcrypt;
use Zend\Form\View\Helper\FormMonthSelect as FormMonthSelect;
use Zend\Http\Request as Request;
use User\Helpers\User as UserHelper;
use Zend\Mvc\ModuleRouteListener;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {

    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    // public function getAutoloaderConfig() { }

    public function getAutoloaderConfig()
    {
        return array(
            /* 'Zend\Loader\ClassMapAutoloader' => array(
                 __DIR__ . '/autoload_classmap.php'
             ),*/

            //the standard autoloader requires a namespace and the path where to find the files for that namespace
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                    'User' => __DIR__ . '/src/User',
                ),
            )
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'User\Model\AuthStorage' => function () {
                        return new AuthStorage('zf');
                },
                'Zend\I18n\Translator\Translator' => function () {
                        return new Translator();
                },
                'User\Model\UserTable' => function ($sm) {
                        $tableGateway = $sm->get('UserTableGateway');
                        $table = new UserTable($tableGateway, $sm);
                        return $table;
                },
                'UserTableGateway' => function ($sm) {
                        $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                        $resultSetPrototype = new ResultSet();
                        $resultSetPrototype->setArrayObjectPrototype(new User());
                        return new TableGateway('users', $dbAdapter, null, $resultSetPrototype);
                },
                'User\Model\PasswordRecoveryTable' => function ($sm) {
                        $tableGateway = $sm->get('PasswordRecoveryTableGateway');
                        $table = new PasswordRecoveryTable($tableGateway);
                        return $table;
                },
                'PasswordRecoveryTableGateway' => function ($sm) {
                        $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                        $resultSetPrototype = new ResultSet();
                        $resultSetPrototype->setArrayObjectPrototype(new PasswordRecovery());
                        return new TableGateway('password_recovery', $dbAdapter, null, $resultSetPrototype);
                },
                'mail.transport' => function () {
                        $options = new SmtpOptions();
                        $options->setHost('10.10.0.114');
                        $options->setPort('2500');
                        $options->setName('php_department');
                        $transport = new SmtpTransport();
                        $transport->setOptions($options);
                        return $transport;
                },
                'mail.message' => function () {
                        $message = new Message();
                        $message->setFrom('y.kostrikova@nixsolutions.com', 'yulia');
                        $message->setTo('baziak@nixsolutions.com', 'baziak');
                        $message->getHeaders()->addHeaderLine('PROJECT', 'zend_project');
                        $message->getHeaders()->addHeaderLine(
                            'EMAILS',
                            'baziak@nixsolutions.com,y.kostrikova@nixsolutions.com'
                        );
                        return $message;
                },
                'bcrypt' => function () {
                        $bcrypt = new Bcrypt();
                        return $bcrypt;
                },


            ),
        );
    }

    public function getViewHelperConfig()
    {

    }
}
