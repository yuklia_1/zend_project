<?php
return array(
    'Registration' => 'Регистрация',
    'Sign in' => 'Вход',
    'Settings' => 'Настройки',
    'Log out' => 'Выход',
    'All users' => 'Все пользователи',
    'Main page' => 'Главная страница',
    'Link expired' => 'Срок действия ссылки истек',
    'Password recovery' => 'Восстановление пароля',
    'After changing password we can' => 'После смены пароля вы можете'
);
