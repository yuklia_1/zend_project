<?php

return array(
    'controllers' => array(
        'invokables' => array(
            'User\Controller\User' => 'User\Controller\UserController',
            'User\Controller\Registration' => 'User\Controller\RegistrationController',
            'User\Controller\Auth' => 'User\Controller\AuthController',
            'User\Controller\UploadFile' => 'User\Controller\UploadFileController',

        ),
    ),
    'view_manager' => array(
        'default_template_suffix' => 'phtml',
        'strategies' => array(
            'ViewJsonStrategy',
        ),
        'template_path_stack' => array(
            'user' => __DIR__ . '/../view',
        ),
    ),
    'view_helpers' => array(

    ),

    'router' => array(
        'routes' => array(
            'signin' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/[:lang]/signin',
                    'defaults' => array(
                        // Change this value to reflect the namespace in which
                        // the controllers for your module are found
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User\Controller\Auth',
                        'action' => 'signin',
                    ),
                    'constraints' => array(
                        'lang' => '[a-z]{2}?',
                    ),
                ),

            ),
            'allProfiles' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/[:lang]/profiles',
                    'defaults' => array(
                        // Change this value to reflect the namespace in which
                        // the controllers for your module are found
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User\Controller\User',
                        'action' => 'allProfiles',
                    ),
                    'constraints' => array(
                        'lang' => '[a-z]{2}?',
                    ),
                ),

            ),
            /*   'showProfile' => array(
                   'type'    => 'Segment',
                   'options' => array(
                       'route'    => '/[:lang]/profile/[:username]',
                       'defaults' => array(
                           // Change this value to reflect the namespace in which
                           // the controllers for your module are found
                           '__NAMESPACE__' => 'User\Controller',
                           'controller'    => 'user',
                           'action'        => 'showProfile',
                       ),
                       'constraints' => array(
                           'lang' => '[a-z]{2}?',
                       ),
                   ),
               ),*/
            'editProfile' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/[:lang]/myprofile',
                    'defaults' => array(
                        // Change this value to reflect the namespace in which
                        // the controllers for your module are found
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User\Controller\User',
                        'action' => 'editProfile',
                    ),
                    'constraints' => array(
                        'lang' => '[a-z]{2}?',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]+',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'selectLanguage' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route' => '/lang',
                            'defaults' => array(
                                //'controller' => 'User\Controller\User',
                                'action' => 'selectLanguage',
                            )
                        )
                    ),
                 ),
              ),
              'changeImage' => array(
                  'type' => 'Literal',
                  'options' => array(
                      'route' => '/image',
                      'defaults' => array(
                          // Change this value to reflect the namespace in which
                          // the controllers for your module are found
                          '__NAMESPACE__' => 'User\Controller',
                          'controller' => 'User\Controller\UploadFile',
                          'action' => 'changeImage',
                      ),

                  ),
              ),

            'signup' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/[:lang]/signup',
                    'defaults' => array(
                        // Change this value to reflect the namespace in which
                        // the controllers for your module are found
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User\Controller\Registration',
                        'action' => 'registration'
                    ),
                    'constraints' => array(
                        'lang' => '[a-z]{2}?',
                    ),
                ),
            ),
            'logout' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/[:lang]/logout',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User\Controller\Auth',
                        'action' => 'logout',
                    ),
                    'constraints' => array(
                        'lang' => '[a-z]{2}?',
                    ),
                ),
            ),
            'recovery' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/[:lang]/recovery',
                    'defaults' => array(

                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User\Controller\Registration',
                        'action' => 'passwordRecovery',
                    ),
                    'constraints' => array(
                        'lang' => '[a-z]{2}?',
                    ),
                ),
            ),
            'change' => array(  // сменить название
                'type' => 'Segment',
                'options' => array(
                    'route' => '/[:lang]/change',
                    'defaults' => array(
                        // Change this value to reflect the namespace in which
                        // the controllers for your module are found
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User\Controller\Registration',
                        'action' => 'changePassword',
                    ),
                    'constraints' => array(
                        'lang' => '[a-z]{2}?',
                    ),
                ),
            ),
            'linkExpired' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/[:lang]/link_expired',
                    'defaults' => array(
                        // Change this value to reflect the namespace in which
                        // the controllers for your module are found
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User\Controller\Registration',
                        'action' => 'changePassword',
                    ),
                    'constraints' => array(
                        'lang' => '[a-z]{2}?',
                    ),
                ),
            ),
        ),
    ),
);
