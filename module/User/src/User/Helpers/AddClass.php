<?php

namespace User\Helpers;

use Zend\View\Helper\AbstractHelper;

class AddClass extends AbstractHelper
{
    /**
     * @param class_value - value of html attribute
     * @param $form
     * @param object of Zend\View\Renderer\PhpRenderer
     */

    public function __invoke($class_value, $form)
    {
        foreach ($form->getElements() as $key => $value) {
            if ($this->getView()->formElementErrors($form->get($key))) {
                $class = $form->get($key)->getAttribute('class');
                if (null == $class) {
                    throw new Exception('Attribute "class" does\'t exists !');
                } elseif ($class == '') {
                    $form->get($key)->setAttributes(array('class' => $class_value));
                } else {
                    $class .= " " . $class_value;
                    $form->get($key)->setAttributes(array('class' => $class));
                }
            }
        }
        return $form;
    }
}
