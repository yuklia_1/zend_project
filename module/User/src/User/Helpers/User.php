<?php

namespace User\Helpers;

use Zend\Http\Request;
use Zend\Http\Response;
use Zend\View\Helper\AbstractHelper;
use Zend\Session\Container as Container;
use Zend\ServiceManager\ServiceLocatorAwareInterface as ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface as ServiceLocatorInterface;
use \Zend\Http\Header\SetCookie as SetCookie;
use Zend\Http\Header\Cookie as Cookie;

class User extends AbstractHelper
{
    private $userSession;
    private $request;
    private $response;

    public function __construct(Request $request, Response $response)
    {
        $this->userSession = new Container('user');
        $this->request = $request;
        $this->response = $response;
    }

    public function __invoke($property)
    {
        switch ($property) {
            case 'lang':
                return $this->getLanguage();
            case 'username':
                return $this->getUsername();
            case 'role':
                return $this->getRole();
            default:
                throw new \Exception('Could not find such property !');
        }
    }


    private function getLanguage()
    {
        $cookie = $this->request->getCookie();
        if (isset($cookie['lang'])) {

            return $cookie['lang'];
        } else {

            $cookie = new SetCookie('lang', 'ru', time() + 365 * 60 * 60 * 24); // now + 1 year
            $response = $this->response->getHeaders();
            $response->addHeader($cookie);
            return 'ru'; //default lang задаем маршрут
        }

    }

    private function getRole()
    {
        if (isset($this->userSession->role)) {

            return $this->userSession->role;
        }
    }


    private function getUsername()
    {
        if (isset($this->userSession->username)) {

            return $this->userSession->username;
        }
    }
}
