<?php
namespace User\Listeners;

use Zend\Debug\Debug;
use Zend\EventManager\ListenerAggregateInterface;
use User\Exception;
use Zend\Mvc\MvcEvent;

class DispatchErrorListener implements ListenerAggregateInterface
{
    /***
     * Attach one or more listeners
     *
     * Implementors may add an optional $priority argument; the EventManager
     * implementation will pass this to the aggregate.
     *
     * @param \Zend\EventManager\EventManagerInterface $events
     *
     * @return void
     */

    public function attach(\Zend\EventManager\EventManagerInterface $events)
    {
        $events->attach(\Zend\Mvc\MvcEvent::EVENT_DISPATCH_ERROR, array($this, 'generateErrorView'));
    }

    /***
     * Detach all previously attached listeners
     *
     * @param \Zend\EventManager\EventManagerInterface $events
     *
     * @return void
     */

    public function detach(\Zend\EventManager\EventManagerInterface $events)
    {
        // TODO: Implement detach() method.
    }

    public function generateErrorView(MvcEvent $events)
    {
        Debug::dump('generateErrorView');
        $exception = $events->getParam('exception');
        Debug::dump($events->getRouteMatch());
        Debug::dump($exception);
        if (!$exception instanceof DispatchErrorException)
        {
            Debug::dump('DispatchErrorException');
            return;
        }

        $model = new \Zend\View\Model\ViewModel(array(
            'message' => $exception->getMessage(),
            'reason' => $exception->getReasonPhrase(),
            'exception' => $exception,
        ));
        $model->setTemplate('error/application_error');
        $events->getViewModel()->addChild($model);

        $response = $events->getResponse();
        $response->setStatusCode($exception->getStatusCode());

        $events->stopPropagation();

        return $model;
    }

}