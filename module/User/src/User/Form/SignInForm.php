<?php

namespace User\Form;

use DluTwBootstrap;
use Zend\Form\Element;
use Zend\Form\Form;

class SignInForm extends Form //Factory-backed Form
{

    public function __construct()
    {
        parent::__construct();
        $this->setName('signin_form');
        $this->setAttribute('method', 'post');

        $this->add(
            array(
                'name' => 'username',
                'attributes' => array(
                    'placeholder' => 'Login',
                    'class' => 'form-control username_tip',
                    'id' => 'username',

                ),
                'options' => array(
                    'label' => 'Логин',
                ),
                'type' => 'Zend\Form\Element\Text',

            )
        );

        $this->add(
            array(
                'type' => 'Zend\Form\Element\Password',
                'name' => 'password',
                'attributes' => array(
                    'placeholder' => 'Password',
                    'class' => 'form-control password_tip',
                    'id' => 'password'
                ),
                'options' => array(
                    'label' => 'Пароль',
                ),
            )
        );
        $this->add(
            array(
                'type' => 'Zend\Form\Element\Checkbox',
                'name' => 'remember',
                'attributes' => array(),
                'options' => array(
                    'label' => 'Remember me',

                ),
            )
        );

        $this->add(
            array(
                'name' => 'b_signin',
                'attributes' => array(
                    'type' => 'Zend\Form\Element\Submit',
                    'value' => 'enter',
                    'class' => 'btn btn-default btn-lg btn-block',
                ),
                'options' => array(
                    'primary' => true,
                ),
            )
        );
        $this->add(
            array(
                'name' => 'b_reg',
                'attributes' => array(
                    'type' => 'Zend\Form\Element\Button',
                    'value' => 'reg',
                    'class' => 'btn btn-primary btn-lg btn-block',
                ),
            )
        );

    }
}
