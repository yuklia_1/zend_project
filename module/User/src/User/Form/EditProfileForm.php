<?php

namespace User\Form;

use Zend\Form\Element;
use Zend\Form\Form;

class EditProfileForm extends Form
{
    public function __construct()
    {
        parent::__construct();
        $this->setAttribute('method', 'post');
        $this->setAttribute('id', 'edit_profile_form');

        $element = new Element\Select('language');
        $element->setAttributes(array('id' => 'select_lang', 'class' => 'form-control', 'selected' => 'Russian'));
        $element->setValueOptions(
            array(
                'ru' => 'Russian',
                'en' => 'English',
            )
        );
        $this->add($element);

        $this->add(
            array(
                'type' => 'Zend\Form\Element\Text',
                'name' => 'username',
                'attributes' => array(
                    'id' => 'username',
                    'class' => 'form-control'
                ),
                'options' => array(
                    'label' => 'Username',
                ),

            )
        );

        $this->add(
            array(
                'type' => 'Zend\Form\Element\Email',
                'name' => 'email',
                'attributes' => array(
                    'id' => 'email',
                    'class' => 'form-control'
                ),
                'options' => array(
                    'label' => 'Email',
                ),

            )
        );
        $this->add(
            array(
                'type' => 'Zend\Form\Element\DateSelect',
                'name' => 'date_of_birth',
                'attributes' => array(
                    'class' => 'form-control selectpicker',
                    'id' => 'date_of_birth',

                ),
                'options' => array(
                    'label' => 'Birthdate',
                    'locale' => 'ru-RU',
                    'dataType' => '0',
                    // 'create_empty_option' => true,
                    'min_year' => date('Y') - 30,
                    'max_year' => date('Y') - 18,
                    'day_attributes' => array(
                        'class' => 'form-control selectpicker select-date', // margin-left: 35px; margin-right:35px
                        'style' => 'width: 25% ; height:20%;  margin-left: 2px; margin-right:2px',
                    ),
                    'month_attributes' => array(
                        'class' => 'form-control selectpicker select-date',
                        'style' => 'width: 40% ; height:20%',
                    ),
                    'year_attributes' => array(
                        'class' => 'form-control selectpicker select-date',
                        'style' => 'width: 27% ; height:20%',
                    )
                )
            )
        );
        $this->add(
            array(
                'type' => 'Zend\Form\Element\Submit',
                'name' => 'b_edit',
                'attributes' => array(
                    'id' => 'b_edit',
                    'type' => 'Zend\Form\Element\Submit',
                    'value' => 'Save changes',
                    'class' => 'btn btn-default btn-lg btn-block',
                ),
                'options' => array(
                    'primary' => true,
                ),
            )
        );

    }
}
