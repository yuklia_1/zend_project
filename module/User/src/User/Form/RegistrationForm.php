<?php

namespace User\Form;

use Zend\Form\Form;

class RegistrationForm extends Form
{
    public function __construct()
    {
        parent::__construct();
        $this->setAttribute('method', 'post');
        $this->setAttribute('id', 'signup_form');
        $this->add(
            array(
                'type' => 'Zend\Form\Element\Text',
                'name' => 'username',
                'attributes' => array(
                    'placeholder' => 'Username',
                    'class' => 'form-control username_tip',
                    'id' => 'username',
                ),
                'options' => array(
                    'label' => 'Your username',
                ),
            )
        );
        $this->add(
            array(
                'type' => 'Zend\Form\Element\Password',
                'name' => 'password',
                'attributes' => array(
                    'placeholder' => 'Password',
                    'class' => 'form-control password_tip',
                    'id' => 'password',
                ),
                'options' => array(
                    'label' => 'Your password',
                ),
            )
        );
        $this->add(
            array(
                'type' => 'Zend\Form\Element\Password',
                'name' => 'password_1',
                'attributes' => array(
                    'placeholder' => 'Repeat password',
                    'class' => 'form-control password_1_tip',
                    'id' => 'password_1',
                ),
                'options' => array(
                    'label' => 'Repeat password',
                ),
            )
        );
        $this->add(
            array(
                'type' => 'Zend\Form\Element\Email',
                'name' => 'email',
                'attributes' => array(
                    'placeholder' => 'Email',
                    'class' => 'form-control email_tip',
                    'id' => 'email',
                ),
                'options' => array(
                    'label' => 'Your email',
                ),
            )
        );

        $this->add(
            array(
                'type' => 'Zend\Form\Element\DateSelect',
                'name' => 'date_of_birth',
                'attributes' => array(
                    'class' => 'form-control selectpicker',
                    'id' => 'date_of_birth',

                ),
                'options' => array(
                    'label' => 'Birthdate',
                    'locale' => 'ru-RU',
                    'dataType' => '0',
                    // 'create_empty_option' => true,
                    'min_year' => date('Y') - 30,
                    'max_year' => date('Y') - 18,
                    'day_attributes' => array(
                        'class' => 'form-control selectpicker select-date', // margin-left: 35px; margin-right:35px
                        'style' => 'width: 25% ; height:20%;  margin-left: 5px; margin-right:5px',
                    ),
                    'month_attributes' => array(
                        'class' => 'form-control selectpicker select-date',
                        'style' => 'width: 40% ; height:20%',
                    ),
                    'year_attributes' => array(
                        'class' => 'form-control selectpicker select-date',
                        'style' => 'width: 27% ; height:20%',
                    )
                )
            )
        );

        $this->add(
            array(
                'name' => 'b_reg',
                'attributes' => array(
                    'type' => 'Zend\Form\Element\Submit',
                    'value' => 'enter',
                    'class' => 'btn btn-default btn-lg btn-block',
                ),
                'options' => array(
                    'primary' => true,
                ),
            )
        );

    }
}
