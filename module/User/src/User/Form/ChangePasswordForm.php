<?php

namespace User\Form;

use Zend\Form\Element;
use Zend\Form\Form;

class ChangePasswordForm extends Form //Factory-backed Form
{

    public function __construct()
    {
        parent::__construct();
        $this->setName('change_password_form');
        $this->setAttribute('method', 'post');

        $this->add(
            array(
                'type' => 'Zend\Form\Element\Password',
                'name' => 'password',
                'attributes' => array(
                    'placeholder' => 'New password',
                    'class' => 'form-control'
                ),
            )
        );
        $this->add(
            array(
                'type' => 'Zend\Form\Element\Password',
                'name' => 'new_password',
                'attributes' => array(
                    'placeholder' => 'Repeat password',
                    'class' => 'form-control'
                ),
            )
        );
        $this->add(
            array(
                'name' => 'b_change',
                'attributes' => array(
                    'type' => 'Zend\Form\Element\Submit',
                    'value' => 'Change',
                    'class' => 'btn btn-default btn-lg btn-block',
                ),
                'options' => array(
                    'primary' => true,
                ),
            )
        );
    }
}
