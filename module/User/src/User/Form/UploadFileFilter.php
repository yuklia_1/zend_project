<?php

namespace User\Form;

use Zend\Debug\Debug;
use Zend\Filter\File\Rename;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Validator\Db\NoRecordExists;
use Zend\InputFilter\FileInput;
use Zend\Validator\File\UploadFile;
use Zend\Validator\File\Extension;
use Zend\Filter\File\RenameUpload as RenameUpload;
use Zend\Http\PhpEnvironment\Request;
use Zend\Validator\File\ImageSize;

class UploadFileFilter implements InputFilterAwareInterface
{
    protected $inputFilter;
    protected $sm;
    private $uploads_rel = '/img/avatars/';


    public function setServiceLocator($sm)
    {
        $this->sm = $sm;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $file = new FileInput('avatar');
            $file->getValidatorChain()
                ->attach(new UploadFile())
                ->attach(new Extension(array('jpeg', 'png')))
                ->attach(
                    new \Zend\Validator\File\Size(array(
                        'min' => '10kB',
                        'max' => '50000kB'
                    ))
                )
                ->attach(
                    new ImageSize(array(
                        'minWidth' => 100,
                        'minHeight' => 100,
                        'maxWidth' => 200,
                        'maxHeight' => 200,
                    ))
                );

            $filter = new RenameUpload(PUBLIC_PATH . $this->uploads_rel);
            $filter->setRandomize(true);
            $filter->setUseUploadName(true);
            $request = new Request();
            $files = $request->getFiles();
            $filter->filter($files['avatar']);
            $file->getFilterChain()
                ->attach($filter);

            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }
}
