<?php

namespace User\Form;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Validator\Db\NoRecordExists;
use Zend\Validator\EmailAddress;
use Zend\Validator\Regex;

class EditProfileFilter implements InputFilterAwareInterface
{
    protected $inputFilter;
    protected $sm;

    /*
     * for database adapter
     */
    public function setServiceLocator($sm)
    {
        $this->sm = $sm;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {

            $inputFilter = new InputFilter();

            $inputFilter->add(
                array(
                    'name' => 'username',
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'NotEmpty',
                            'options' => array(),
                        ),
                        array(
                            'name' => 'Zend\Validator\Db\NoRecordExists',
                            'options' => array(
                                'table' => 'users',
                                'field' => 'username',
                                'adapter' => $this->sm->get('Zend\Db\Adapter\Adapter'),
                                'messages' => array(
                                    NoRecordExists::ERROR_RECORD_FOUND => 'Login has already exist',
                                ),
                            ),
                        ),
                        array(
                            'name' => 'Regex',
                            'options' => array(
                                'pattern' => '/^[A-Za-z][A-Za-z0-9]*(?:_[A-Za-z0-9]+)*$/',
                                'messages' => array(
                                    Regex::NOT_MATCH => 'The input does not match against rules',
                                ),

                            ),
                        ),
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min' => '3',
                                'max' => '32',

                            ),
                        ),
                    ),
                )
            );

            $inputFilter->add(
                array(
                    'name' => 'email',
                    'filters' => array(
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'NotEmpty',
                            'options' => array(),
                        ),
                        array(
                            'name' => 'Zend\Validator\Db\NoRecordExists',
                            'options' => array(
                                'table' => 'users',
                                'field' => 'email',
                                'adapter' => $this->sm->get('Zend\Db\Adapter\Adapter'),
                                'messages' => array(
                                    NoRecordExists::ERROR_RECORD_FOUND => 'Email has already exist',
                                ),

                            ),
                        ),
                        array(
                            'name' => 'EmailAddress',
                            'options' => array(),
                            'messages' => array(
                                EmailAddress::INVALID_FORMAT => 'Email does not match against pattern',
                            ),
                        ),

                    )
                )
            );


            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }
}
