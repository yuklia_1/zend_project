<?php

namespace User\Form;

use Zend\Form\Form;

class RecoveryPasswordForm extends Form
{
    public function __construct()
    {
        parent::__construct();
        $this->setAttribute('method', 'post');

        $this->add(
            array(
                'type' => 'Zend\Form\Element\Email',
                'name' => 'email',
                'attributes' => array(
                    'placeholder' => 'Enter your email',
                    'class' => 'form-control'
                ),
                'options' => array(
                    'label' => '',
                ),
            )
        );
        $this->add(
            array(
                'name' => 'b_recovery',
                'attributes' => array(
                    'type' => 'Zend\Form\Element\Submit',
                    'value' => 'enter',
                    'class' => 'btn btn-default btn-lg btn-block',
                ),
                'options' => array(
                    'primary' => true,
                ),
            )
        );
    }
}
