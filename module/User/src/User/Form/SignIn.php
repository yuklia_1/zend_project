<?php

namespace User\Form;

class SignIn extends \Zend\Form\Form
{
    public function __construct()
    {
        parent::__construct('SignIn');
        $this->setAttribute('action', '/signin');
        $this->setAttribute('method', 'post');
        $this->add(
            array(
                'type' => 'Zend\Form\Element\Text',
                'name' => 'username',
                'options' => array(
                    'label' => 'Your username',
                ),
            )
        );
        $this->add(
            array(
                'type' => 'Zend\Form\Element\Password',
                'name' => 'password_1',
                'options' => array(
                    'label' => 'Your password',
                ),
            )
        );
        $this->add(
            array(
                'type' => 'Zend\Form\Element\Button',
                'name' => 'ok',

            )
        );
    }
}
