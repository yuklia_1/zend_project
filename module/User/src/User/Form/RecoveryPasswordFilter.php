<?php

namespace User\Form;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class RecoveryPasswordFilter implements InputFilterAwareInterface
{
    protected $inputFilter;
    protected $sm;

    /*
     * for database adapter
     */
    public function setServiceLocator($sm)
    {
        $this->sm = $sm;
    }


    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();


            $inputFilter->add(
                array(
                    'name' => 'email',
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                              \Zend\Validator\NotEmpty::IS_EMPTY => 'Поле не должно быть пустым!',
                                ),
                            ),
                        ),
                        array(
                            'name' => 'EmailAddress',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\EmailAddress::INVALID_FORMAT => 'Неверный email!',
                                ),
                            ),
                        ),
                        array(
                            'name' => 'Zend\Validator\Db\RecordExists',
                            'options' => array(
                                'table' => 'users',
                                'field' => 'email',
                                'adapter' => $this->sm->get('Zend\Db\Adapter\Adapter'),
                                'messages' => array(
                \Zend\Validator\Db\RecordExists::ERROR_NO_RECORD_FOUND => 'Email не зарегистрирован !',
                                ),
                            ),
                        ),

                    )
                )
            );
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
}
