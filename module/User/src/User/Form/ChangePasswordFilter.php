<?php

namespace User\Form;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ChangePasswordFilter implements InputFilterAwareInterface
{

    protected $inputFilter; // <-- Add this variable

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $inputFilter->add(
                array(
                    'name' => 'password',
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                              \Zend\Validator\NotEmpty::IS_EMPTY => 'Поле не должно быть пустым!',
                                ),
                            ),
                        ),
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min' => '8',
                                'max' => '16',
                                'messages' => array(
                    \Zend\Validator\StringLength::INVALID => 'Длинна должна быть в пределах!',
                    \Zend\Validator\StringLength::TOO_SHORT => 'Длинна <8 !',
                                ),

                            ),
                        ),
                        array(
                            'name' => 'Regex',
                            'options' => array(
                                'pattern' => '/^[A-Za-z][A-Za-z0-9]*(?:_[A-Za-z0-9]+)*$/',
                                'messages' => array(
                         \Zend\Validator\Regex::NOT_MATCH => 'Поле не соответствует шаблону!',
                                ),
                            ),
                        ),
                    ),
                )
            );
            $inputFilter->add(
                array(
                    'name' => 'new_password',
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                              \Zend\Validator\NotEmpty::IS_EMPTY => 'Поле не должно быть пустым!',
                                ),
                            ),
                        ),
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min' => '8',
                                'max' => '16',
                                'messages' => array(
                    \Zend\Validator\StringLength::INVALID => 'Длинна должна быть в пределах!',
                    \Zend\Validator\StringLength::TOO_SHORT => 'Длинна <8 !',
                                ),

                            ),
                        ),
                        array(
                            'name' => 'Regex',
                            'options' => array(
                                'pattern' => '/([a-zA-Z]+[0-9]+)|([0-9]+[a-zA-Z]+)/',
                                'messages' => array(
                         \Zend\Validator\Regex::NOT_MATCH => 'Поле не соответствует шаблону!',
                                ),
                            ),
                        ),
                        array(
                            'name' => 'Identical',
                            'options' => array(
                                'token' => 'password',
                                'messages' => array(
                                    \Zend\Validator\Identical::NOT_SAME => 'Пароли не совпадают!',
                                ),
                            ),
                        ),
                    ),
                )
            );
            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }
}
