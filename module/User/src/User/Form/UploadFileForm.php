<?php

namespace User\Form;

use Zend\Form\Element;
use Zend\Form\Form;

class UploadFileForm extends Form
{
    public function __construct()
    {
        parent::__construct();
        $this->setAttribute('method', 'post');
        $this->setAttribute('id', 'upload_image_form');
        $this->setAttribute('enctype', 'multipart/form-data');


        $file = new Element\File();
        $file->setOptions(array())
            ->setName('avatar')
            ->setLabel('Change image')
            ->setAttribute('id', 'fileupload');

        $this->add($file);

    }
}
