<?php

namespace User\Form;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Mvc\I18n\Translator as Translator;
use Zend\Validator\Regex;

class SignInFilter implements InputFilterAwareInterface
{
    protected $inputFilter; // <-- Add this variable

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $translator = new Translator();

            $inputFilter->add(
                array(
                    'name' => 'username',
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'NotEmpty',
                            'options' => array(
                                'translator' => $translator,
                            ),
                        ),
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'translator' => $translator,
                                'encoding' => 'UTF-8',
                                'min' => '3',
                                'max' => '32',
                            ),
                        ),
                        array(
                            'name' => 'Regex',
                            'options' => array(
                                'translator' => $translator,
                                'pattern' => '/^[A-Za-z][A-Za-z0-9]*(?:_[A-Za-z0-9]+)*$/',
                                'messages' => array(
                                    Regex::NOT_MATCH => 'The input does not match against rules',
                                ),

                            ),
                        ),
                    ),
                )
            );
            $inputFilter->add(
                array(
                    'name' => 'password',
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'NotEmpty',
                            'options' => array(
                                'translator' => $translator,
                            ),
                        ),
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'translator' => $translator,
                                'encoding' => 'UTF-8',
                                'min' => '8',
                                'max' => '32',
                            ),
                        ),
                        array(
                            'name' => 'Regex',
                            'options' => array(
                                'translator' => $translator,
                                'pattern' => '/([a-zA-Z]+[0-9]+)|([0-9]+[a-zA-Z]+)/',
                                'messages' => array(
                                    Regex::NOT_MATCH => 'The input does not match against rules',
                                ),

                            ),
                        ),
                    ),
                )
            );
            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }
}
