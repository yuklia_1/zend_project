<?php
namespace User\Model;

class PasswordRecovery
{
    public $id_password_recovery;
    public $id_user;
    public $link_expire;
    public $secret_word;

    //allows data from an external source to be mapped to the internal properties of the object
    public function exchangeArray($data)
    {
        if (isset($data['secret_word'])) {
            $this->secret_word = $data['secret_word'];
        } else {
            $this->secret_word = null;
        }

        if (isset($data['id_user'])) {
            $this->id_user = $data['id_user'];
        } else {
            $this->id_user = null;
        }

        if (isset($data['link_expire'])) {
            $this->link_expire = $data['link_expire'];
        } else {
            $this->link_expire = null;
        }

    }
    /*
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
*/
}
