<?php
namespace User\Model;

use Zend\Authentication\Storage;

class AuthStorage extends Storage\Session
{
    public function rememberMe($time = 1209600)
    {
        $this->session->getManager()->rememberMe($time);
    }

    public function forgetMe()
    {
        $this->session->getManager()->forgetMe();
    }
}
