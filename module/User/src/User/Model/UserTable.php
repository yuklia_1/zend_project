<?php
namespace User\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Debug\Debug;

class UserTable //implements TableGatewayInterface
{
    protected $tableGateway;
    protected $sm;

    public function __construct(TableGateway $tableGateway, $sm)
    {
        $this->tableGateway = $tableGateway;
        $this->sm = $sm;
    }

    public function getUserByEmail($email)
    {
        $rowset = $this->tableGateway->select(array('email' => $email));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row with $email");
        }
        return $row;
    }

    public function getUserById($id)
    {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }


    public function select($where = null)
    {

    }

    public function getUserByName($username)
    {
        $rowset = $this->tableGateway->select(array('username' => $username));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $username");
        }
        return $row;

    }

    public function insertUser(User $user)
    {

        $bcrypt = $this->sm->get('bcrypt');
        $securePass = $bcrypt->create($user->password);
        $data = array(
            'username' => $user->username,
            'password' => $securePass,
            'email' => $user->email,
            'date_of_birth' => $user->date_of_birth
        );

        $id = (int)$user->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getUserById($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('User id does not exist');
            }
        }

    }

    public function updatePassword($password, $id_user)
    {
        var_dump($id_user);
        $bcrypt = $this->sm->get('bcrypt');
        $securePass = $bcrypt->create($password);
        $data = array(
            'password' => $securePass,
        );
        $this->tableGateway->update($data, array('id' => $id_user));
    }

    public function delete($where)
    {

    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }
}
