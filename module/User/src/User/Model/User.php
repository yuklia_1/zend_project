<?php

namespace User\Model;

use \Zend\Stdlib\ArrayObject\PhpReferenceCompatibility;

class User extends PhpReferenceCompatibility
{
    public $id;
    public $username;
    public $email;
    public $password;
    public $date_of_birth;
    public $avatar;
    public $date_create;
    public $date_update;
    public $active;
    public $role;

    /**
     * @param array|\Zend\Stdlib\ArrayObject\ArrayObject $data
     * @return array|void
     */
    public function exchangeArray($data)
    {
        //parent::exchangeArray($data);

        if (isset($data['username'])) {

            $this->username = $data['username'];
        } else {

            $this->username = null;
        }

        if (isset($data['password'])) {

            $this->password = $data['password'];
        } else {

            $this->password = null;
        }

        if (isset($data['email'])) {

            $this->email = $data['email'];
        } else {

            $this->email = null;
        }

        if (isset($data['date_of_birth'])) {

            $this->date_of_birth = $data['date_of_birth'];
        } else {

            $this->date_of_birth = null;
        }
    }

    /**
     * @return array
     */
    public function getArrayCopy()
    {
        return parent::getArrayCopy();
        //return get_object_vars($this);
    }
}
