<?php
namespace User\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\TableGatewayInterface as TableGatewayInterface;

class PasswordRecoveryTable //implements TableGatewayInterface
{
    protected $tableGateway;


    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function insertUserId($id_user)
    {
        $data = array('id_user' => $id_user);
        $this->tableGateway->insert($data);
    }

    public function getSecretWordByUserId($id_user)
    {
        $rowset = $this->tableGateway->select(array('id_user' => $id_user));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row with $id_user");
        }
        return $row->secret_word;
    }

    public function getRowBySecretWord($secret_word)
    {
        $rowset = $this->tableGateway->select(array('secret_word' => $secret_word));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row with $secret_word");
        }
        return $row;
    }
}
