<?php
namespace User\Letter;

use Zend\Mail;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\Mail\Transport as Smtp;

/**
 * class for send letter via smtp protocol
 */
class Letter
{
    private $sm;
    private $lang;
    private $secret_word;

    public function __construct($sm, $lang, $secret_word)
    {
        $this->sm = $sm;
        $this->lang = $lang;
        $this->secret_word = $secret_word;
    }

    public function send()
    {
        $readyLetter = $this->composeLetter($this->createLetterBody($this->lang, $this->secret_word));
        $transport = $this->sm->get('mail.transport');
        $message = $this->sm->get('mail.message');
        $message->setBody($readyLetter);
        $transport->send($message);
    }

    public function createLetterBody($lang, $secret_word)
    {
        $body = '<p><h1>Go through in order to change password</h1>
        <a href="http://zend_project.yuklia.croco.nixsolutions.com/'
            . $lang . '/change?secret_word=' . urlencode(
                $secret_word
            ) . '"' . '>Change password</a></p>';
        return $body;
    }

    public function composeLetter($body, $htmlPart = null, $textPart = null)
    {
        if (null == $htmlPart) {
            $htmlPart = new MimePart($body);
        }
        if (null == $textPart) {
            $textPart = new MimePart($body);
        }
        $htmlPart->type = "text/html";
        $textPart->type = "text/plain";
        $mimeMessage = new MimeMessage();
        $mimeMessage->setParts(array($textPart, $htmlPart));
        return $mimeMessage;
    }
}
