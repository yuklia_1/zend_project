<?php

/**
 * AuthAdapter
 */

namespace User\Adapters;

use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Authentication\Result as Result;
use Zend\Debug\Debug;

/**
 * Class AuthAdapter
 * @package User\Adapters
 */
class AuthAdapter implements AdapterInterface
{
    /**
     * @var AdapterInterface
     */
    private $dbAdapter;
    /**
     * @var
     */
    private $username;
    /**
     * @var
     */
    private $password;
    /**
     * @var
     */
    private $sm;

    /**
     * @param $dbAdapter
     * @param $username
     * @param $password
     * @param $sm
     */
    public function __construct($dbAdapter, $username, $password, $sm)
    {
        $this->username = $username;
        $this->password = $password;
        $this->dbAdapter = $dbAdapter;
        $this->sm = $sm;

    }

    /**
     * Performs an authentication attempt
     *
     * @return \Zend\Authentication\Result
     * @throws \Zend\Authentication\Adapter\Exception\ExceptionInterface
     *               If authentication cannot be performed
     */



    public function authenticate()
    {
        /**
         * @var $r \Zend\Db\ResultSet\ResultSet
         */

        $bcrypt = $this->sm->get('bcrypt');
        $userRow = $this->dbAdapter->query("SELECT * FROM users WHERE username = ? ", array($this->username));
        if (!empty($userRow)) {

            $securePassword = $userRow->current()['password'];
            if ($bcrypt->verify($this->password, $securePassword)) {

                return new Result(Result::SUCCESS, $this->username, array());
            } else {

                return new Result(
                    Result::FAILURE,
                    $this->username,
                    array('fail' => 'Sorry, but you are not authorized yet')
                );
            }
        } else {

            return new Result(
                Result::FAILURE,
                $this->username,
                array('fail' => 'Sorry, but you are not authorized yet')
            );
        }
    }
}
