<?php

namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use User\Form\SignInForm as SignInForm;
use User\Form\SignInFilter as SignInFilter;
use User\Model\User as User;
use Zend\View\Model\ViewModel;
use User\Adapters\AuthAdapter as AuthAdapter;
use Zend\Authentication\AuthenticationService;
use Zend\Mail;
use Zend\Session\Container as Container;
use Zend\Debug as Debug;

class AuthController extends AbstractActionController
{
    protected $userTable;
    protected $passwordRecoveryTable;
    protected $translator;

    public function signinAction()
    {
        $session = new Container('user');
        if (isset($session->role)) {

            if ($session->role != 'guest') {

                return $this->redirect()->toRoute('allProfiles', array('lang' => $this->getLang()));
            }
        }
        $user = new User();
        $form = new SignInForm();
        $form->get('b_signin')->setValue('Вход');
        $request = $this->getRequest();
        $messages = '';
        $receivedData = false;
        if ($request->isPost()) {

            $receivedData = true;
            $formFilter = new SignInFilter();
            $form->setInputFilter($formFilter->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {

                $user->exchangeArray($form->getData());
                $sm = $this->getServiceLocator();
                $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                $authStorage = $sm->get('User\Model\AuthStorage');
                $authAdapter = new AuthAdapter($dbAdapter, $user->username, $user->password, $sm);
                $auth = new AuthenticationService();
                $auth->setStorage($authStorage);
                $result = $auth->authenticate($authAdapter);
                if ($result->isValid()) {

                    $session = new Container('user');
                    $session->role = 'member';
                    $session->username = $auth->getIdentity();
                    if ($request->getPost('remember')) {

                        $authStorage->rememberMe();
                        $auth->setStorage($authStorage);
                    }
                    return $this->redirect()->toRoute('allProfiles', array('lang' => $this->getLang()));
                }
                foreach ($result->getMessages() as $message) {

                    $messages .= "$message\n";
                }
            }

        }

        $view = new ViewModel(array(
            'form' => $form,
            'messages' => $messages,
            'receivedData' => $receivedData,
        ));
        $view->setTemplate('user/auth/signin');
        return $view;
    }

    public function logoutAction()
    {
        $sm = $this->getServiceLocator();
        $authStorage = $sm->get('User\Model\AuthStorage');
        $auth = new AuthenticationService();
        $auth->clearIdentity();
        $authStorage->forgetMe();
        $auth->setStorage($authStorage);
        $session = new Container('user');
        if (isset($session->role)) {

            $session->role = 'guest';
        }

        return $this->redirect()->toRoute('home', array('lang' => $this->getLang()));
    }

    public function getPasswordRecoveryTable()
    {
        if (!$this->passwordRecoveryTable) {

            $sm = $this->getServiceLocator();
            $this->passwordRecoveryTable = $sm->get('User\Model\PasswordRecoveryTable');
        }
        return $this->passwordRecoveryTable;
    }

    public function getUserTable()
    {
        if (!$this->userTable) {

            $sm = $this->getServiceLocator();
            $this->userTable = $sm->get('User\Model\UserTable');
        }
        return $this->userTable;
    }

    private function getLang()
    {
        return $this->getEvent()->getRouteMatch()->getParam('lang');
    }
}
