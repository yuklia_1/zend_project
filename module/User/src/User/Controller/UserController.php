<?php

namespace User\Controller;

use User\Form\EditProfileFilter;
use User\Form\EditProfileForm;
use User\Form\UploadFileFilter;
use User\Form\UploadFileForm;
use Zend\Debug\Debug;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Stdlib\ArrayObject;
use Zend\View\Helper\Gravatar;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Zend\Authentication\AuthenticationService;
use Zend\Mail;
use Zend\Session\Container as Container;
use Zend\Stdlib\ErrorHandler as ErrorHandler;
use \Zend\Http\Header\SetCookie as SetCookie;
use Zend\Http\Header\Cookie as Cookie;

class UserController extends AbstractActionController
{
    protected $userTable;
    protected $passwordRecoveryTable;
    const IMAGE_SIZE = 200;
    const FILE_NAME = 'avatar';

    public function allProfilesAction()
    {
        $auth = new AuthenticationService();
        $currentUser = $auth->getStorage()->read();
        $view = new ViewModel(array(
            'results' => $this->getUserTable()->fetchAll(),
            'currentUser' => $currentUser,
        ));
        $view->setTemplate('user/user/allProfiles');
        return $view;
    }

    public function showProfileAction()
    {
    }

    public function uploadProgressAction()
    {
        $id = $this->params()->fromQuery('id', null);
        $progress = new \Zend\ProgressBar\Upload\SessionProgress();
        return new \Zend\View\Model\JsonModel($progress->getProgress($id));
    }

    public function selectLanguageAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $lang = $request->getPost('lang');
            //  $session = new Container('user');
            // $session->lang = $lang;
            $cookie = new SetCookie('lang', $lang, time() + 365 * 60 * 60 * 24, '/'); // now + 1 year
            $response = $this->getResponse()->getHeaders();
            $response->addHeader($cookie);
            $url = $this->getEvent()->getRouter()->assemble(array('lang' => $lang), array('name' => 'editProfile'));

            return new JsonModel(array(
                'data' => $url
            ));

        }
        $viewModel = new ViewModel();
        $viewModel->setTemplate('user/user/selectLanguage');
        $viewModel->setTerminal($request->isXmlHttpRequest());
        return $viewModel;
    }


    public function editProfileAction()
    {
        $formImage = new UploadFileForm();
        $gravatar = $this->loadGravatar();
        $sm = $this->getServiceLocator();
        $receivedData = false;
        $isValid = false;
        $session = new Container('user');
        $formProfile = new EditProfileForm();
        $formProfileFilter = new EditProfileFilter();

        $sm = $this->getServiceLocator();
        $request = $this->getRequest();

        if ($request->isPost()) {

            $receivedData = true;
            $formProfileFilter->setServiceLocator($sm);
            $formProfile->setInputFilter($formProfileFilter->getInputFilter());
            $formProfile->setData($request->getPost());
            if ($formProfile->isValid()) {

                $isValid = true;
                $formProfile->get('b_edit')->setValue("changed");
                //  $user->exchangeArray($form->getData());
                //insert id role
                //$this->getUserTable()->insertUser($user);

            }
        } else {

            $user = $this->getUserTable()->getUserByName($session->username);
            $formProfile->bind($user);
        }
        $view = new ViewModel(array(
            'formImage' => $formImage,
            'form' => $formProfile,
            'receivedData' => $receivedData,
            'email' => $gravatar['email'],
            'options' => $gravatar['options']

        ));
        $view->setTemplate('user/user/editProfile');
        return $view;
    }

    public function loadGravatar()
    {
        $result = array();
        $session = new Container('user');
        $user = $this->getUserTable()->getUserByName($session->username);
        $result['email'] = $user->email;
        $result['options'] = array(
            'img_size' => self::IMAGE_SIZE,
            'default_img' => Gravatar::DEFAULT_MM,
            'rating' => Gravatar::RATING_G,
            'secure' => null,
        );
        return $result;
    }

    public function testGravatar()
    {
        $result = array();
        $result['email'] = 'yuklia.pur@gmail.com';
        $result['options'] = array(
            'img_size' => self::IMAGE_SIZE,
            'default_img' => Gravatar::DEFAULT_MM,
            'rating' => Gravatar::RATING_G,
            'secure' => null,
        );
        return $result;
    }


    public function getPasswordRecoveryTable()
    {
        if (!$this->passwordRecoveryTable) {

            $sm = $this->getServiceLocator();
            $this->passwordRecoveryTable = $sm->get('User\Model\PasswordRecoveryTable');
        }
        return $this->passwordRecoveryTable;
    }

    public function getUserTable()
    {
        if (!$this->userTable) {

            $sm = $this->getServiceLocator();
            $this->userTable = $sm->get('User\Model\UserTable');
        }
        return $this->userTable;
    }
}
