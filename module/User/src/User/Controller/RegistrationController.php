<?php

namespace User\Controller;

use Zend\Debug\Debug;
use Zend\Http\Response;
use Zend\Mvc\Controller\AbstractActionController;
use User\Form\ChangePasswordFilter;
use User\Form\ChangePasswordForm;
use User\Form\RegistrationForm as RegistrationForm;
use User\Form\RecoveryPasswordForm as RecoveryPasswordForm;
use User\Form\RegistrationFilter as RegistrationFilter;
use User\Form\RecoveryPasswordFilter as RecoveryPasswordFilter;
use User\Model\User as User;
use Zend\Authentication\Storage\Session as Session;
use Zend\View\Model\ViewModel;
use Zend\Mail;
use User\Letter\Letter as Letter;
use Zend\Session\Container;
use User\Exception\DispatchErrorException;

class RegistrationController extends AbstractActionController
{
    protected $userTable;
    protected $passwordRecoveryTable;
    protected $translator;

    public function registrationAction()
    {
        $receivedData = false;
        $user = new User();
        $form = new RegistrationForm();
        $formFilter = new RegistrationFilter();
        $form->get('b_reg')->setValue('Registration');
        $request = $this->getRequest();
        if ($request->isPost()) {

            $receivedData = true;
            $sm = $this->getServiceLocator();
            $formFilter->setServiceLocator($sm);
            $form->setInputFilter($formFilter->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {

                //$session = new Container('user');
                // $session->role = 'member';
                $user->exchangeArray($form->getData());
                //insert id role
                $this->getUserTable()->insertUser($user);
            }
        }

        $view = new ViewModel(array(
            'form' => $form,
            'receivedData' => $receivedData,
        ));
        $view->setTemplate('user/registration/registration'); //если с разных слов то дефис
        return $view;
    }

    public function passwordRecoveryAction()
    {
        $receivedData = false;
        $form = new RecoveryPasswordForm();
        $formFilter = new RecoveryPasswordFilter();
        $user = new User();
        $request = $this->getRequest();
        if ($request->isPost()) {

            $receivedData = true;
            $sm = $this->getServiceLocator();
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $formFilter->setServiceLocator($sm);
            $form->setInputFilter($formFilter->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {

                $user->exchangeArray($form->getData());
               //$row=$this->getUserTable()->getUserByEmail($user->email);//посмотереть почему id =null
                $id_user = $dbAdapter->query("SELECT id FROM users WHERE email = ?", array($user->email));
                $id_user = $id_user->current()['id']; //?
                $this->getPasswordRecoveryTable()->insertUserId($id_user);
                $secret_word = $this->getPasswordRecoveryTable()->getSecretWordByUserId($id_user);
                $lang = $this->getEvent()->getRouteMatch()->getParam('lang');
                $letter = new Letter($sm, $lang, $secret_word);
                $letter->send();
            }
        }

        $view = new ViewModel(array(
            'form' => $form,
            'receivedData' => $receivedData
        ));
        $view->setTemplate('user/registration/passwordrecovery'); //если с разных слов то дефис
        return $view;

    }


    public function changePasswordAction()
    {
        $receivedData = false;
        $form = new ChangePasswordForm();
        $formFilter = new ChangePasswordFilter();
        $request = $this->getRequest();
        $ses = new Session();
        if ($request->isGet()) {

            $session = new Container('link');
            $secret_word = $request->getQuery('secret_word');
            try {

                $row = $this->getPasswordRecoveryTable()->getRowBySecretWord($secret_word);
            } catch (\Exception $e) {

                throw new DispatchErrorException(
                    'Uri has errors',
                    'error-controller-cannot-dispatch',
                    Response::STATUS_CODE_404
                );
            }

            $ses->write($row);
            if ($row != null) {

                $session->date_of_link_expire = $row->link_expire;
                if ($this->isLinkExpire()) {

                    return $this->redirect()->toRoute('linkExpired', array('lang' => $this->getLang()));
                }
            }
        }
        if ($request->isPost()) {

            $receivedData = true;
            if ($this->isLinkExpire()) {

                return $this->redirect()->toRoute('linkExpired', array('lang' => $this->getLang()));
            }

            $user = new User();
            $form->setInputFilter($formFilter->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {

                $user->exchangeArray($form->getData());
                $this->getUserTable()->updatePassword($user->password, $ses->read()->id_user);
            }
        }
        $view = new ViewModel(array(
            'form' => $form,
            'receivedData' => $receivedData
        ));
        $view->setTemplate('user/registration/changePassword');
        return $view;
    }


    public function isLinkExpire()
    {
        $session = new Container('link');
        $current_date = date("Y-m-d H:i:s");
        if ($session->date_of_link_expire > $current_date) {

            return true;
        } else {

            return false;
        }
    }

    public function getPasswordRecoveryTable()
    {
        if (!$this->passwordRecoveryTable) {

            $sm = $this->getServiceLocator();
            $this->passwordRecoveryTable = $sm->get('User\Model\PasswordRecoveryTable');
        }
        return $this->passwordRecoveryTable;
    }

    public function getUserTable()
    {
        if (!$this->userTable) {

            $sm = $this->getServiceLocator();
            $this->userTable = $sm->get('User\Model\UserTable');
        }
        return $this->userTable;
    }

    private function getLang()
    {
        return $this->getEvent()->getRouteMatch()->getParam('lang');
    }
}
