<?php

namespace User\Controller;

use User\Form\UploadFileFilter;
use User\Form\UploadFileForm;
use Zend\Debug\Debug;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class UploadFileController extends \Zend\Mvc\Controller\AbstractActionController
{
    public $session;
    private $uploads_rel = '/img/avatars/';

    public function changeImageAction()
    {
        $filedata = array();
        $form = new UploadFileForm();
        $formFilter = new UploadFileFilter();
        $form->setInputFilter($formFilter->getInputFilter());
        $request = $this->getRequest();
        if ($request->isPost()) {

            $post = array_merge_recursive(
                $request->getPost()->toArray(),
                $request->getFiles()->toArray()
            );

            $form->setData($post);
            if ($form->isValid($post)) {

                $data = $form->getData();
            } else {

                $fileErrors = $form->getMessages();
            }

            return new JsonModel(array(
                'type' => $post['avatar']['type'],
                // 'url'=>$post['avatar']['name'],
                //'thumbnail_url'=>'',
                'name' => $post['avatar']['name'],
                'size' => $post['avatar']['size'],
                'delete_url' => '',
                'DELETE' => 'DELETE',
            ));
        }
        $viewModel = new ViewModel();
        $viewModel->setTemplate('user/user/editProfile');
        $viewModel->setTerminal($request->isXmlHttpRequest());
        return $viewModel;
    }
}
