<?php
namespace User\Exception;

use \Exception;

class DispatchErrorException extends \Exception
{
    private $reasonPhrase;
    private $statusCode;

    public function __construct($message, $reasonPhrase = null, $statusCode = null)
    {
        parent::__construct($message);
    }

    /**
     * @return mixed
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @return mixed
     */
    public function getReasonPhrase()
    {
        return $this->reasonPhrase;
    }
}
