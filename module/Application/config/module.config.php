<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'controllers' => array(
        'invokables' => array(
            'Application\Controller\Index' => 'Application\Controller\IndexController',
            'Application\Controller\Translator' => 'Application\Controller\TranslatorController'
        ),
    ),
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action' => 'index',


                    ),
                    'constraints' => array(
                        'lang' => '[a-z]{2}?',
                    ),
                ),
            ),
            'zero' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/zero',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action' => 'pageZero',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'one' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route' => '/one',
                            'defaults' => array(
                               // 'controller' => 'Application\Controller\Index',
                                'action' => 'pageOne',
                            )
                        )
                    ),
                    'two' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route' => '/two',
                            'defaults' => array(
                               // 'controller' => 'Application\Controller\Index',
                                'action' => 'pageTwo',
                            )
                        )
                    ),

                )


            ),

            'login' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/[:lang]/login',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action' => 'login',

                    ),
                    'constraints' => array(
                        'lang' => '[a-z]{2}?',
                    ),
                ),
            ),
            'translate' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/translate',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Translator',
                        'action' => 'translate',

                    ),
                    'constraints' => array(
                        'lang' => '[a-z]{2}?',
                    ),
                ),
            ),
            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /application/:controller/:action
            'application' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/application',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller' => 'Index',
                        'action' => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    /*  'translator' => array(
          //'locale' => 'ru_RU',
          'translation_file_patterns' => array(
              array(
                  'type'     => 'gettext',
                  'base_dir' => __DIR__ . '/../language',
                  'pattern'  => '%s.mo',
              ),
              array(
                  'type'     => 'phparray',
                  'base_dir' => __DIR__ . '/../../User/language',
                  'pattern'  => '%s.php',
                  'text_domain' => 'user'
              ),
          ),
      ),*/

    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => array(
            'layout/layout' => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml',
        ),
        'default_template_suffix' => 'phtml',
        'strategies' => array(
            'ViewJsonStrategy',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(),
        ),
    ),
);
