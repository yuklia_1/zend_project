<?php
return array(
    'Value is required and can\'t be empty' => 'Поле не может быть пустым',
    'The input is less than 8 characters long' => 'Поле < 8 символов',
    'The input is more than 16 characters long' => 'Поле > 16 символов',
    'The input does not match against pattern' => 'Поле не соответствует шаблону',
    'The input does not match against rules' => 'Данные введены некоректно',
    'A record matching the input was found' => 'Такая запись уже существует',
    'Email does not match against pattern' => 'Ошибочный email',
    'Email has already exist' => 'Этот email уже зарегистрирован',
    'Login has already exist' => 'Этот логин уже существует',
    'Passwords aren\'t equal' => 'Пароли не совпадают',
    'At least 1 digit, 1 char' => 'По крайней мере 1 цифра , 1 буква',
    'Field length should be min 3 max 32' => 'Длинна поля должна быть min 3, max 32',
    'Field length should be min 3' => 'Длинна поля должна быть min 3',
    'Sign up' => 'Регистрация',
    'Sign in' => 'Вход',
    'Settings' => 'Настройки',
    'Log out' => 'Выход',
    'Log in' => 'Вход',
    'All users' => 'Все пользователи',
    'Main page' => 'Главная страница',
    'Link expired' => 'Срок действия ссылки истек',
    'Password recovery' => 'Восстановление пароля',
    'After changing password we can' => 'После смены пароля вы можете',
    'Remember me' => 'Запомнить меня',
    'Forgot password' => 'Забыли пароль',
    'Invalid input' => 'Ошибочный ввод',
    'Login' => 'Логин',
    'Password' => 'Пароль',
    'Username' => 'Имя пользователя',
    'All profiles' => 'Все профили',
    'Greetings' => 'Добро пожаловать',
    'Guest' => 'гость',
    'Edit profile' => 'Редактировать профиль',
    'Simple project' => 'Учебный проект',
    'You have already log in' => 'Вы уже вошли в свой аккаунт',
    'Warning' => 'Внимание',
    'Repeat password' => 'Повторите пароль',
    'Email' => 'Почта',
    'Date of birthday' => 'Дата рождения',
    'Form data is valid' => 'Данные валидны',
    'Now we can' => 'Теперь вы можете',
    'Link was sent to your email. Link\'ll be active during 3 days.' =>
    'Ссылка отправлена вам на email.
     Она действительна в течении 3 дней.',
    'Sorry, but you are not authorized yet' =>
    'Вы не прошли аутентификацию',
    'At least 1 char .Min 3, max 32' =>
    'По крайне мере 1 символ . Минимальная длинна - 3
     символа, максимальная - 32 ',
    'At least 1 digit and 1 char. Min length 8 characters' =>
    'По крайней мере 1 цифра и 1 символ.
     Минимальная длинна поля 8 символов'
);
