<?php
return array(
    'Регистрация' => 'Registration',
    'Вход' => 'Sign in',
    'Настройки' => 'Settings',
    'Выход' => 'Log out',
    'Все пользователи' => 'All users',
    'Главная страница' => 'Main page',
    'Срок действия ссылки истек' => 'Link expired',
    'Восстановление пароля' => 'Password recovery',
    'После смены пароля ви можете' => 'After changing password we can'
);
