<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        return new ViewModel();
    }

    public function loginAction()
    {
        $view = new ViewModel();
        $view->setTemplate('application/index/login');
        return $view;
    }
    public function pageZeroAction()
    {
        $view = new ViewModel();
        $view->setTemplate('application/index/pageZero');
        return $view;
    }
    public function pageOneAction()
    {
        $view = new ViewModel();
        $view->setTemplate('application/index/pageOne');
        return $view;
    }
    public function pageTwoAction()
    {
        $view = new ViewModel();
        $view->setTemplate('application/index/pageTwo');
        return $view;
    }
}
