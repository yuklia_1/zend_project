<?php

namespace Application\Controller;

use Zend\Debug\Debug;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class TranslatorController extends AbstractActionController
{
    public function translateAction()
    {
        $request = $this->getRequest();
        $sm = $this->getServiceLocator();
        $translator = $sm->get('translator');
        if ($request->isPost()) {

            $message = $this->params()->fromPost('content');
            $result = $translator->translate($message);
            return new JsonModel(array(
                'result' => $result
            ));
        }


        $viewModel = new ViewModel();
        $viewModel->setTemplate('/application/translator/translator');
        $viewModel->setTerminal($request->isXmlHttpRequest());
        return $viewModel;
    }
}
