<?php

namespace Application\Listeners;

use Zend\EventManager\EventInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\Mvc\MvcEvent;
use Zend\Session\Container;
use Zend\Validator\AbstractValidator as AbstractValidator;

class LocaleListener implements ListenerAggregateInterface
{

    protected $listeners = array();

    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach(MvcEvent::EVENT_ROUTE, array($this, 'initLocale'), 1);
    }

    public function detach(EventManagerInterface $events)
    {
        foreach ($this->listeners as $index => $listener) {

            if ($events->detach($listener)) {

                unset($this->listeners[$index]);
            }
        }
    }

    public function doEvent(EventInterface $event)
    {
        echo 'param id  = ' . $event->getParam('id');
    }

    public function initLocale(MvcEvent $e)
    {
        $translator = $e->getApplication()->getServiceManager()->get('translator');
        $config = $e->getApplication()->getServiceManager()->get('Config');
        $cookies = $e->getApplication()->getRequest()->getCookie();
        $shotLang = $cookies['lang'];

        if (isset($config['languages'][$shotLang])) {

            $translator->setLocale($config['languages'][$shotLang]['locale']);
            $file = "lang.array." . $shotLang . ".php";
            $translator->addTranslationFile(
                'phparray',
                realpath(__DIR__ . "/../../../" . '/language/' . $file)
            ); //переместить в ресурсы
            AbstractValidator::setDefaultTranslator($translator);
        } else {
            /*
             *  $cookies = $e->getApplication()->getRequest()->getCookie();
             *  $cookies['lang']
             */
            $lang = array_shift($config['languages']);
            $translator->setLocale($lang['locale']);
        }
    }
}
