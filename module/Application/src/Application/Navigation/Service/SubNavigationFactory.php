<?php
namespace Application\Navigation\Service;

use Zend\Navigation\Service\DefaultNavigationFactory;

class SubNavigationFactory extends DefaultNavigationFactory
{
    protected function getName()
    {
        return 'sub';
    }
}