<?php

namespace Acl\Listeners;

use Zend\EventManager\EventInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\Mvc\MvcEvent;
use Zend\Session\Container;

class AclListener implements ListenerAggregateInterface
{
    protected $acl;

    protected $listeners = array();

    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach(MvcEvent::EVENT_ROUTE, array($this, 'checkAccess'));
    }

    public function detach(EventManagerInterface $events)
    {
        foreach ($this->listeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->listeners[$index]);
            }
        }
    }

    public function doEvent(EventInterface $event)
    {
        echo 'param id  = ' . $event->getParam('id');
    }

    public function checkAccess($e)
    {
        /**
         *  @var $app \Zend\Mvc\Application
         */
        $accessToSpecificPrivilege = false;
        $accessToAllResources =false;
        $route = $e->getRouteMatch();
        $app=$e->getApplication();
        $session = new Container('user');
        $role = isset($session->role) ? $session->role : $session->role = 'guest';
        $this->acl = $app->getServiceManager()->get('acl');
        if (!$this->acl->hasResource($route->getParam('controller'))) {
            $accessToSpecificPrivilege = false;
        } else {
            $accessToAllResources = $this->acl->isAllowed($role, $route->getParam('controller'), 'all');
            if (!$accessToAllResources) {

                $accessToSpecificPrivilege = $this->acl->isAllowed(
                    $role,
                    $route->getParam('controller'),
                    $route->getParam('action')
                );
            }

        }
        if (!($accessToSpecificPrivilege || $accessToAllResources)) {

            //  $this->redirectToHome($e);
        }
    }


    public function redirectToHome($e)
    {
        $response = $e->getResponse();
        $response->setStatusCode(302);
        $response->getHeaders()->addHeaderLine('Location', '/');
        return $response;
    }
}
