<?php

namespace Acl;

use Zend\Debug\Debug;
use Zend\Permissions\Acl\Acl;
use Zend\Permissions\Acl\Resource\GenericResource as Resource;
use Zend\Permissions\Acl\Role\GenericRole as Role;
use Zend\Session\Container;

class Module
{
    protected $acl;

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                    'Acl' => __DIR__ . '/src/Acl',
                ),
            )
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'acl' => function ($sm) {
                      $this->acl = new Acl();
                      $roleGuest = new Role('guest');
                      $this->acl->addRole($roleGuest);
                      $this->acl->addRole(new Role('member'));
                      $this->acl->addRole(new Role('admin'), 'member');
                      $config = $sm->get('Config');
                      $allowResources = $config['resources']['acl_for_module_user']['allow'];
                      //  Debug::dump($allowResources);
                    //  $privileges = $config['privileges']['acl_for_module_user'];
                    foreach ($allowResources as $resource => $arrayOfRolePrivileges) {

                        if (!$this->acl->hasResource($resource)) {

                                $this->acl->addResource(new Resource($resource));
                        }

                        foreach ($arrayOfRolePrivileges as $role => $privileges) {

                            if (is_array($privileges)) {

                                foreach ($privileges as $key) {

                                    $this->acl->allow($role, $resource, $key);
                                }

                            } else {

                                $this->acl->allow($role, $resource, 'all');
                            }
                        }
                    }
                        return $this->acl;
                }
            )
        );
    }
}
