/**
 * Created by yuklia on 2/25/14.
 */
/**
 * Created by yuklia on 2/12/14.
 */
var fields = {};
var elementTip = {};

translate('At least 1 char .Min 3, max 32');
elementTip['username_tip'] = $('<ul></ul>').html("<li>" + translated_message + "</li>");
translate('At least 1 digit and 1 char. Min length 8 characters');
elementTip['password_tip'] = $('<ul></ul>').html("<li>" + translated_message + "</li>");

$(document).ready(function () {
    for (var id in elementTip) {
        addTipsPopover(elementTip[id], id);
    }
});

function glueMessages(obj) {
    $messages = '';
    for (var error in obj) {
        var alert_field = obj[error];
        console.log('glueMessages: ' + alert_field);
        $messages += alert_field + " ";
    }
    return $messages;
}


function addErrorPopover(content, id_element) {
    console.log("Content: " + content);
    var attr = {};
    attr['content'] = content;
    attr['placement'] = 'right';
    attr['toggle'] = 'popover';
    attr['trigger'] = 'hover';
    attr['html'] = 'true';
    $(document).ready(function () {
        $('#' + id_element).popover('destroy');  //смена сообщений
        $("#" + id_element).popover(attr).popover('show');
        $("#" + id_element).addClass("input-error");
    });
}

function addTipsPopover(content, class_element) {
    var attr = {};
    attr['content'] = content;
    attr['placement'] = 'right';
    attr['toggle'] = 'popover';
    attr['trigger'] = 'hover';
    attr['html'] = 'true';
    $(document).ready(function () {
        $("." + class_element).hover(function () {
                $("." + class_element).popover(attr).popover('show');
            }
        )
    });
}


function setContentTip(content) {
    var attr = {};
    attr['content'] = content;
    attr['placement'] = 'right';
    attr['toggle'] = 'popover';
    attr['trigger'] = 'hover';
    attr['html'] = 'true';
    return attr;
}


function deleteTips() {
    for (var element in elementTip) {
        $("input").removeClass(element);
    }

}


$(document).ready(function () {
    $("#username").focus();
    $("#signin_form").submit(function (event) {
        var data_ok = true;
        translated_message = '';
        deleteTips();
        for (var property in fields) {
            delete fields[property];
        }
        var username = $("#username").val();

        Validator.field_value = username;
        translate('Value is required and can\'t be empty');
        Validator.add('empty', 'true',
            {
                message: translated_message
            }
        );
        translate('Field length should be min 3 max 32');
        Validator.add('length_between', '',
            {
                max: 32,
                min: 3,
                message: translated_message
            });
        translate('The input does not match against pattern');
        Validator.add('field_pattern', '',
            {
                pattern: '^[A-Za-z][A-Za-z0-9]*(?:_[A-Za-z0-9]+)*$',
                message: translated_message
            });

        var username_message = Validator.run();
        if (!$.isEmptyObject(username_message)) {
            data_ok = false;
            fields['username'] = glueMessages(username_message);
        } else {
            $("#username").removeClass("input-error");
            $('#username').popover('destroy');

        }

        var password_1 = $("#password").val();
        Validator.field_value = password_1;
        translate('Value is required and can\'t be empty');
        Validator.add('empty', 'true',
            {
                message: translated_message
            }
        );
        translate('Field length should be min 3');
        Validator.add('length_between', '',  //?
            {
                max: 32,
                min: 3,
                message: translated_message
            });
        translate('At least 1 digit, 1 char');
        Validator.add('field_pattern', '',
            {
                pattern: '([a-zA-Z]+[0-9]+)|([0-9]+[a-zA-Z]+)',
                message: translated_message
            });

        var password_1_message = Validator.run();
        if (!$.isEmptyObject(password_1_message)) {
            data_ok = false;
            fields['password'] = glueMessages(password_1_message);
        } else {
            $("#password").removeClass("input-error");
            $('#password').popover('destroy');

        }

        if (!data_ok) {
            for (var id in fields) {
                var alert_field = fields[id];
                addErrorPopover(alert_field, id);
            }
        }
        console.log(data_ok);
        return data_ok; // отправка данных на сервер*/
    });
});



