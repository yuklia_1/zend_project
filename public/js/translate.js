/**
 * Created by yuklia on 2/24/14.
 */
function translate(message) {
    $.ajax({
        url: "/translate",
        type: "POST",
        data: {
            content: message
        },
        dataType: "json",
        async: false,
        success: function (data) {
            window.translated_message = data.result;
        },
        fail: function (data) {

        }
    });

}

