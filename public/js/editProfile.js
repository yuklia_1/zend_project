/**
 * Created by yuklia on 2/21/14.
 */

$(document).ready(function () {

    $("#select_lang").change(function () {
        var lang = $("#select_lang option:selected").val();

        $.ajax({
            type: "POST",
            url: '/ru/myprofile/selectLanguage',  ///???
            data: { lang: lang},
            success: function (response, textStatus) {
                if (response) {
                    var parser = JSON.stringify(response);
                    var obj = jQuery.parseJSON(parser);
                    window.location.href = obj.data;

                }
            }
        })
    });

});


