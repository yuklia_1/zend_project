<?php
return array(

    'resources' => array(
        'acl_for_module_user' => array(
            'allow' => array(
                'Application\Controller\Translator' => array('guest' => 'all'),
                'Application\Controller\Index' => array('guest' => 'all'),
                'User\Controller\User' => array('member' => array('allProfiles', 'editProfile'), 'admin' => 'all'),
                'User\Controller\Registration' => array('guest' => 'all'),
                'User\Controller\Auth' => array('guest' => array('signin'), 'member' => array('logout')),
                'User\Controller\UploadFile' => array('member' => 'all', 'admin' => 'all'),
            ),
            'deny' => array()
        ),

    )
);
