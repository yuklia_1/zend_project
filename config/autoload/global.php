<?php
return array(
    'db' => array(
        'driver' => 'Pdo',
        'dsn' => 'mysql:host=hippo.nixsolutions.com;port=3306;dbname=u_student_mems',
        'username' => 'p_memes',
        'password' => 'ueWieK3k',
        'hostname' => 'hippo.nixsolutions.com',
        'driver_options' => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
        ),
    ),

    /* 'path'=>array(
         'language'=>''
     ),*/

    'languages' => array(
        'ru' => array(
            'name' => 'russian',
            'locale' => 'ru_RU',
        ),
        'en' => array(
            'name' => 'english',
            'locale' => 'en_US',
        ),
    ),

    'view_helpers' => array(
        'invokables' => array(
            'AddClass' => 'User\Helpers\AddClass',
        ),
        'factories' => array(
            // the array key here is the name you will call the view helper by in your view scripts
            'user' => function ($sm) {
                    $locator = $sm->getServiceLocator(); // $sm is the view helper manager, so we need to fetch the main service manager
                    return new User\Helpers\User($locator->get('request'), $locator->get('response'));
                },
        ),
    ),

);