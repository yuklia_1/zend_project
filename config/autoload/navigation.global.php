<?php
return array( // ToDO make it dynamic - comes from the DB
    'navigation' => array(
        'sub' => array(
            array(
                'label' => 'Home',
                'route' => 'home',
                'controller' => 'index',
                'action' => 'index',
                'resource' => 'Application\Controller\Index',
                // 'privilege'	=> 'home',
            ),
            array(
                'label' => 'Zero',
                'route' => 'zero',
                'controller' => 'Application\Controller\Index',
                // 'action'	=> 'index',
                // 'resource' => 'Application\Controller\Index',
                //'privilege'	=> 'home',
                'pages' => array(
                    array(
                        'label' => 'One',
                        'route' => 'zero/one'
                    ),
                    array(
                        'label' => 'Two',
                        'route' => 'zero/two'
                    )
                )
            ),
            array(
                'label' => 'All profiles',
                'route' => 'allProfiles',
                'controller' => 'user',
                'action' => 'allProfiles',
                'resource' => 'User\Controller\User',
                // 'privilege'	=> 'allProfiles',
            ),
            array(
                'label' => 'My URI page',
                'uri' => 'http://www.example.com/',
            ),
        ),
        'main' => array(
            array(
                'label' => 'Edit profile',
                'route' => 'editProfile',
                'use route match'=>true

            ),
            array(
                'label' => 'Log out',
                'route' => 'logout',
                'use route match'=>true
            ),
        ),
    ),

);

